const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const connection = new Sequelize(dbConfig);

//MODELOS
const Usuario = require('../models/User')
const Professor = require('../models/Prof')
const Permissao = require('../models/Permission')

//INICIALIZACAO DOS MODELOS
Usuario.init(connection)
Permissao.init(connection)
Professor.init(connection)

//RELACIOANAMENTOS

//ID de Permissao Vai Para Usuario
Permissao.hasMany(Usuario);
Usuario.belongsTo(Permissao);

//SYNC MODELOS
Professor.sync()
Permissao.sync()
Usuario.sync()

module.exports = connection;